package hu.zth.comp.addrbook.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.AnnotationIntrospectorPair;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.*;

class StatSerializationTest {

    @Test
    @DisplayName("Serialization")
    void test_serialization() throws IOException {
        LocalDateTime lastUpdated = LocalDateTime.of(2018, Month.FEBRUARY, 10, 14, 45, 35);

        Stat cs = Stat.create("stat type name");
        cs.setId(1L);
        cs.setVersion(2L);
        cs.setCount(3L);
        cs.setLastUpdated(Date.from(lastUpdated.atZone(ZoneId.systemDefault()).toInstant()));

        StringWriter sw = new StringWriter();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setAnnotationIntrospector(
                new AnnotationIntrospectorPair(
                        new JacksonAnnotationIntrospector(),
                        new JaxbAnnotationIntrospector(TypeFactory.defaultInstance())));
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        ObjectWriter writer = objectMapper.writer().forType(Stat.class);
        writer.writeValue(sw, cs);
        
        String jsonString = sw.toString();

        assertAll(
                () -> assertNotNull(jsonString),
                () -> assertFalse(jsonString.contains("\"version\" : "), "Must not contain \"version\" property"),
                () -> assertTrue(jsonString.contains("\"id\" : \"#1\""), "Invalid format for \"id\""),
                () -> assertTrue(jsonString.contains("\"lastUpdated\" : \"2018-02-10 14:45:35"), "Invalid date format for \"lastUpdated\"")
        );
    }

}