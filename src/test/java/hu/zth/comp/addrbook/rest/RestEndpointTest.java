package hu.zth.comp.addrbook.rest;

import hu.zth.comp.addrbook.SwarmMain;
import hu.zth.comp.addrbook.model.Contacts;
import hu.zth.comp.addrbook.service.StatelessStateTestService;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.arquillian.CreateSwarm;
import org.wildfly.swarm.undertow.internal.DefaultWarDeploymentFactory;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(Arquillian.class)
public class RestEndpointTest {

    @Inject
    private RestEndpoint restEndpoint;

    @Inject
    private StatelessStateTestService statelessStateTestService;

    @Test
    @DisplayName("Test XML export")
    public void test_xml_export() {
        Response response = restEndpoint.exportAsXml();

        assertAll(
                () -> assertNotNull(response),
                () -> assertNotNull(response.getEntity()),
                () -> assertTrue(response.getEntity() instanceof Contacts),
                () -> assertNotNull(((Contacts) response.getEntity()).getContacts()),
                () -> assertEquals(5, ((Contacts) response.getEntity()).getContacts().size())
        );
    }

    @Test
    @RunAsClient
    @DisplayName("Test XML export format check")
    public void test_xml_export_format_check() throws Exception {
        String xml;

        URL url = new URL("http://localhost:8080/export");
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            xml = reader.lines().collect(Collectors.joining("\n"));
        }

        assertNotNull(xml);

        Document document;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8))) {
            document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(bis);
        }

        XPath xPath = XPathFactory.newInstance().newXPath();

        String company1name = (String) xPath.compile("/contacts/company[@id='#1']/@companyName").evaluate(document, XPathConstants.STRING);
        NodeList company1addressesContacts = (NodeList) xPath.compile("/contacts/company[@id='#1']/addresses[@id='#4']/contacts").evaluate(document, XPathConstants.NODESET);

        String company1addresses4contactId1 = (String) xPath.compile("/contacts/company[@id='#1']/addresses[@id='#4']/contacts[1]/text()").evaluate(document, XPathConstants.STRING);
        String company1addresses4contactId2 = (String) xPath.compile("/contacts/company[@id='#1']/addresses[@id='#4']/contacts[2]/text()").evaluate(document, XPathConstants.STRING);
        Set<String> company1addresses4contactIds = new HashSet<>(Arrays.asList(company1addresses4contactId1, company1addresses4contactId2));

        Node company1email6 = (Node) xPath.compile("/contacts/company[@id='#1']/emails[@id='#6']").evaluate(document, XPathConstants.NODE);
        String company1email6contactId = (String) xPath.compile("/contacts/company[@id='#1']/emails[@id='#6']/contact/text()").evaluate(document, XPathConstants.STRING);

        String person3firstName = (String) xPath.compile("/contacts/person[@id='#3']/@firstName").evaluate(document, XPathConstants.STRING);
        String person3LastName = (String) xPath.compile("/contacts/person[@id='#3']/@lastName").evaluate(document, XPathConstants.STRING);
        Node person3addressLondon = (Node) xPath.compile("/contacts/person[@id='#3']/addresses[city/text()='London']").evaluate(document, XPathConstants.NODE);
        String person3emailWork = (String) xPath.compile("/contacts/person[@id='#3']/emails[type/text()='WORK']/email/text()").evaluate(document, XPathConstants.STRING);

        assertAll(
                () -> assertEquals("TheCompany Ltd.", company1name),
                () -> assertNotNull(company1addressesContacts),
                () -> assertEquals(2, company1addressesContacts.getLength()),
                () -> assertAll(
                        () -> assertTrue(company1addresses4contactIds.contains("#1")),
                        () -> assertTrue(company1addresses4contactIds.contains("#5"))
                ),
                () -> assertNotNull(company1email6),
                () -> assertEquals("#1", company1email6contactId),

                () -> assertEquals("John", person3firstName),
                () -> assertEquals("Doe", person3LastName),
                () -> assertNotNull(person3addressLondon),
                () -> assertEquals("employee@company.hu", person3emailWork)
        );
    }

    @Test
    @DisplayName("Test @Stateless instances")
    public void test_stateless_instances() throws Exception {
        
        int threadCount = 16;
        int taskCount = 64;
        int taskDelayMs = 32;

        List<StatelessStateTestService.RunInfo> infos =
                Collections.synchronizedList(new ArrayList<>());

        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
        CountDownLatch countDownLatch = new CountDownLatch(taskCount);
        for (int i = 0; i < taskCount; i++) {
            executorService.submit(() -> {
                infos.add(statelessStateTestService.touch());
                countDownLatch.countDown();
            });
            LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(taskDelayMs));
        }
        countDownLatch.await();
        executorService.shutdown();

        int createdThreadCount = infos.stream().
                collect(Collectors.groupingBy(StatelessStateTestService.RunInfo::getName)).
                keySet().size();
        
        long multiCallCount = infos.stream().
                filter(runInfo -> runInfo.getInstanceCounter() > 1).count();
        
        int maxInstanceCounter = infos.stream().
                mapToInt(StatelessStateTestService.RunInfo::getInstanceCounter).
                max().orElse(0);
        
        assertEquals(taskCount, infos.size());
        assertEquals(threadCount, createdThreadCount);
        assertTrue(multiCallCount > 0, "There should be at least one instance with multiple calls");
        
        assertTrue(maxInstanceCounter > 24, "Módosítsd a threadCount, taskCount vagy a taskDelayMs változó értékét úgy, hogy a teszt sikeresen lefusson");
        
        /*
            Plusz pontért fejtsd ki pár mondatban,
            hogy miért történik az, ami:
            
            
            .............................................
         */
    }


    @Deployment
    public static Archive createDeployment() throws Exception {
        return DefaultWarDeploymentFactory.archiveFromCurrentApp().addPackage("org.opentest4j");
    }

    @CreateSwarm
    public static Swarm newContainer() throws Exception {
        return new Swarm(false)
                .withConfig(SwarmMain.class.getClassLoader().getResource("project-defaults.yml"));
    }
}