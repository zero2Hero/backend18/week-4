package hu.zth.comp.addrbook;

import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.undertow.internal.DefaultWarDeploymentFactory;

public class SwarmMain {

    public static void main(String[] args) throws Exception {
        new Swarm(false, args)
                .withConfig(SwarmMain.class.getClassLoader().getResource("project-defaults.yml")).
                start(DefaultWarDeploymentFactory.archiveFromCurrentApp());
    }
    
}
