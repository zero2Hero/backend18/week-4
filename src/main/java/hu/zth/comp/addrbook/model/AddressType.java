package hu.zth.comp.addrbook.model;

public enum AddressType {
    
    HEADQUARTER, WORK, OFFICE, HOME
    
}
