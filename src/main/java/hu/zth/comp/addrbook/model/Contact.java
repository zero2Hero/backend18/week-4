package hu.zth.comp.addrbook.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NamedQueries({
        @NamedQuery(name = Contact.ALL_CONTACTS, query = "select c from Contact c"),
        @NamedQuery(name = Contact.CONTACTS_BY_CITY, query = "select c from Contact c join c.addresses a where a.city = :city")
})
@Entity
@DiscriminatorColumn(name = "CONTACT_TYPE")
@Inheritance(strategy = InheritanceType.JOINED)
public class Contact extends BaseEntity {

    public static final String ALL_CONTACTS = "get_all_contacts";
    public static final String CONTACTS_BY_CITY = "get_contacts_by_city";

    @ManyToMany(mappedBy = "contacts", cascade = CascadeType.ALL)
    private Set<Address> addresses = new HashSet<>();

    @OneToMany(mappedBy = "contact", cascade = CascadeType.ALL)
    private Set<Email> emails = new HashSet<>();

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public Set<Email> getEmails() {
        return emails;
    }

    public void setEmails(Set<Email> emails) {
        this.emails = emails;
    }
}
