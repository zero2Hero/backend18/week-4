package hu.zth.comp.addrbook.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@NamedQueries(@NamedQuery(
        name = Stat.BY_TYPE,
        query = "select c from Stat c where c.statName = :statName"))
@Entity
@Table(name = "stat")
public class Stat extends BaseEntity {

    public static final String BY_TYPE = "get_stat_by_name";

    @Column(name = "stat_name", nullable = false, length = 50)
    private String statName;

    @Column(name = "count", nullable = false)
    private Long count = 0L;

    @Column(name = "last_updated", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    @Version
    @Column(name = "version")
    private Long version;

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getStatName() {
        return statName;
    }

    public void setStatName(String statName) {
        this.statName = statName;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public void increaseCount() {
        count++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Stat stat = (Stat) o;
        return Objects.equals(getStatName(), stat.getStatName()) &&
                Objects.equals(getCount(), stat.getCount()) &&
                Objects.equals(getLastUpdated(), stat.getLastUpdated()) &&
                Objects.equals(getVersion(), stat.getVersion());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getStatName(), getCount(), getLastUpdated(), getVersion());
    }

    @Override
    public String toString() {
        return "Stat{" +
                "statName='" + statName + '\'' +
                ", count=" + count +
                ", lastUpdated=" + lastUpdated +
                '}';
    }

    public static Stat create(String statName) {
        Stat stat = new Stat();
        stat.setStatName(statName);
        return stat;
    }

    @PrePersist
    @PreUpdate
    private void ensureLastUpdated() {
        setLastUpdated(new Date());
    }

}
