package hu.zth.comp.addrbook.model;

import java.io.Serializable;

public class StatMessage implements Serializable {
    
    private Class<? extends Contact> contactType;

    public Class<? extends Contact> getContactType() {
        return contactType;
    }
    
    public static StatMessage create(Class<? extends Contact> contactType) {
        StatMessage statMessage = new StatMessage();
        statMessage.contactType = contactType;
        return statMessage;
    }
}
