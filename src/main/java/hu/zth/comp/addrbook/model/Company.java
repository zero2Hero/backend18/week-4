package hu.zth.comp.addrbook.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Objects;

@Entity
@DiscriminatorValue("C")
public class Company extends Contact {
    
    @Column(nullable = false, length = 100)
    private String companyName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Company company = (Company) o;
        return Objects.equals(getCompanyName(), company.getCompanyName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getCompanyName());
    }
}

