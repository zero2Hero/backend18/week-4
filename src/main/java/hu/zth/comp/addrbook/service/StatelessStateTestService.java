package hu.zth.comp.addrbook.service;

import javax.ejb.Stateless;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;

@Stateless
public class StatelessStateTestService {
    
    public static class RunInfo {
        private String name;
        private int instanceId;
        private int instanceCounter;

        public String getName() {
            return name;
        }

        public int getInstanceId() {
            return instanceId;
        }

        public int getInstanceCounter() {
            return instanceCounter;
        }

        @Override
        public String toString() {
            return "Thread name: " + name + ", instance id: " + instanceId + ", instance counter: " + instanceCounter;
        }
        
        public static RunInfo of(String name, int instanceCounter, int sameInstanceCallCounter) {
            RunInfo info = new RunInfo();
            info.name = name;
            info.instanceId = instanceCounter;
            info.instanceCounter = sameInstanceCallCounter;
            return info;
        }
    }

    private static final long DEFAULT_TASK_WAIT_TIME_MS = 500;
    private static final AtomicInteger instanceCounter = new AtomicInteger();
    private Integer instanceId;
    private Integer counter;

    public StatelessStateTestService() {
        instanceId = instanceCounter.incrementAndGet();
        counter = 0;
    }

    public RunInfo touch() {
        counter++;
        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(DEFAULT_TASK_WAIT_TIME_MS));
        return RunInfo.of(Thread.currentThread().getName(), instanceId, counter++);
    }

}
