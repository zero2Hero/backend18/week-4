package hu.zth.comp.addrbook.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Stateless
public class BaseDao {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    public <T> List<T> getAll(Class<T> entityClass) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(entityClass);
        query.from(entityClass);
        return entityManager.createQuery(query).getResultList();
    }
    
    public <T> void createNew(T entity) {
        entityManager.persist(entity);
    }
    
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
}
