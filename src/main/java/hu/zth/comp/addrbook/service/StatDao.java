package hu.zth.comp.addrbook.service;

import hu.zth.comp.addrbook.model.Stat;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Stateless
public class StatDao {
    
    @EJB
    private BaseDao baseDao;
    
    public Optional<Stat> getStatByName(String statName) {
        TypedQuery<Stat> query = baseDao.getEntityManager().createNamedQuery(Stat.BY_TYPE, Stat.class);
        query.setParameter("statName", statName);

        try {
            return Optional.of(query.getSingleResult());
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }
    
    public void saveNew(Stat stat) {
        if (stat.getId() == null) {
            baseDao.createNew(stat);
        }
    }
    
}
