package hu.zth.comp.addrbook.rest;

import hu.zth.comp.addrbook.model.*;
import hu.zth.comp.addrbook.service.BaseDao;
import hu.zth.comp.addrbook.service.StatelessStateTestService;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Topic;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@ApplicationScoped
@Path("/")
public class RestEndpoint {
	
	@EJB
	private BaseDao dao;

    @Inject
    private JMSContext context;
    
    @EJB
    private StatelessStateTestService statelessStateTestService;

    @Resource(lookup = "java:/jms/topic/stat-topic")
    private Topic topic;
    

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public Response doGet() {
		return Response.ok("ZeroToHero verseny 2018 - backend - feladat 4").build();
	}

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listContacts() {
		return Response.ok(dao.getAll(Contact.class)).build();
	}
	
	@GET
	@Path("/new/{firstName}/{lastName}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response createPerson(@NotNull @PathParam("firstName") String firstName, @NotNull @PathParam("lastName") String lastName) {
		Person p = new Person();
		p.setFirstName(firstName);
		p.setLastName(lastName);
		dao.createNew(p);
		context.createProducer().send(topic, context.createObjectMessage(StatMessage.create(Person.class)));
		return Response.ok(String.format("Person created with name: %s %s", firstName, lastName)).build();
	}

    @GET
    @Path("/newCompany/{companyName}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response createCompany(@NotNull @PathParam("companyName") String companyName) {
        Company c = new Company();
        c.setCompanyName(companyName);
        dao.createNew(c);
        context.createProducer().send(topic, context.createObjectMessage(StatMessage.create(Company.class)));
        return Response.ok(String.format("Company created with name: %s", companyName)).build();
    }

	@GET
	@Path("/list/{city}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByCity(@PathParam("city") String city) {
		TypedQuery<Contact> byCityQuery = dao.getEntityManager().createNamedQuery(Contact.CONTACTS_BY_CITY, Contact.class);
		byCityQuery.setParameter("city", city);
		List<Contact> result = byCityQuery.getResultList();
		return Response.ok(result).build();
	}
	
	@GET
    @Path("/export")
    @Produces(MediaType.APPLICATION_XML)
    public Response exportAsXml() {
	    Contacts contacts = new Contacts(dao.getEntityManager().createNamedQuery(Contact.ALL_CONTACTS, Contact.class).getResultList());
	    return Response.ok(contacts).build();
    }

    @GET
    @Path("/stat")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStats() {
        return Response.ok(dao.getAll(Stat.class)).build();
    }
    
    @GET
    @Path("/statelessTouch")
    @Produces(MediaType.TEXT_PLAIN)
    public StatelessStateTestService.RunInfo statelessTouch() {
	    return statelessStateTestService.touch();
    } 
	
}