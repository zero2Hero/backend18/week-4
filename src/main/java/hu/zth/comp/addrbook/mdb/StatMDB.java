package hu.zth.comp.addrbook.mdb;

import hu.zth.comp.addrbook.model.Stat;
import hu.zth.comp.addrbook.model.StatMessage;
import hu.zth.comp.addrbook.service.StatDao;
import org.jboss.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

@MessageDriven(name = "StatMDB", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:/jms/topic/stat-topic"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")})
public class StatMDB implements MessageListener {
    
    @EJB
    private StatDao statDao;
    
    @Override
    public void onMessage(Message message) {
        try {
            StatMessage statMessage = message.getBody(StatMessage.class);
            String statName = statMessage.getContactType().getSimpleName();
            Stat stat = statDao.getStatByName(statName).orElse(Stat.create(statName));
            stat.increaseCount();
            statDao.saveNew(stat);

            /*
            Az 5. feladat leírása szerint fejtsd ki pár mondatban a statDao.saveNew(stat); működését:
            
            
            .............................................
            */
        } catch (JMSException e) {
            Logger.getLogger(StatMDB.class).error("Could not process JMS message", e);
        }
    }

}
